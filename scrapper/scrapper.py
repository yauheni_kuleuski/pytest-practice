from bs4 import BeautifulSoup as BS
import requests
import re
import time
from .websites import JobsTutBySite


class Parser:
    def __init__(self):
        self.enqueuer_urls = []
        self.result_data_list = []
        self.website = JobsTutBySite()
        self.request_keyword = str(input('Please input' \
            ' request keyword $ '))
        searched_words = str(input('Please input' \
            ' 3 searched words (with space) $ '))
        self.searched_words = searched_words.split()[:3]
    

    def appropriate_search_pattern(self):
        '''If we use "WebSite" class, we don't have search_pattern.'''
        self.website.search_pattern = str(input('Please input' \
            ' the job search page $ '))
        

    def find_vacancies(self):
        '''
        Using the entered keyword, we find the url of the vacancy address.
        '''
        page = 0
        while True:
            time.sleep(1)
            print('RUN Page ' + str(page))
            pattern = self.website.search_pattern.format(
                key=self.request_keyword, 
                page=page)
            responce = requests.get(pattern, headers=self.website.headers)
            soup = BS(responce.text, "lxml")
            try:
                vacancy_page_list = soup.find('div', {'class': 'vacancy-serp'})
                if vacancy_page_list.contents:
                    for vacancy in vacancy_page_list:
                        try:
                            url = vacancy.find('a', {'class': 'bloko-link ' \
                                'HH-LinkModifier'}).get('href')
                            self.enqueuer_urls.append(url)
                        except Exception:
                            url = ''
                    print('Page ' + str(page) + ' Done.')
                    page += 1
                else:
                    break
            except Exception:
                pass


    def find_count_words(self, url):
        '''
        For each url we find the search words in the text of the vacancy.
        '''
        time.sleep(1)
        responce = requests.get(url, headers=self.website.headers)
        result_data_dict = {'url': url,
                            'request_keyword': self.request_keyword}
        soup = BS(responce.text, "lxml")
        vacancy_text_block = soup.find('div', {'class': "bloko-columns-row"}) \
            .find_all('div', {'class': "bloko-columns-row"})[:4]
        for div in vacancy_text_block:
            lines_of_text = div.find_all(text=True)
            for line in lines_of_text:
                for word in self.searched_words:
                    result = len(re.findall(word, line, flags=re.IGNORECASE))
                    result_data_dict[word] = \
                        result_data_dict.get(word, 0) + result
        return result_data_dict


    def average_word_occurrence(self):
        '''Calculate the average word occurrence'''
        word1 = 0
        word2 = 0
        word3 = 0
        for dataset in self.result_data_list:
            word1 += dataset[self.searched_words[0]]
            word2 += dataset[self.searched_words[1]]
            word3 += dataset[self.searched_words[2]]
        word1 /= len(self.result_data_list)
        word2 /= len(self.result_data_list)
        word3 /= len(self.result_data_list)
        print('\n{0} vacancies choosed for "{1}"'.format(
            len(self.result_data_list),
            self.request_keyword
        ))
        print('\nAverage word occurrence:\n' \
        '\t' + str(self.searched_words[0]) + ' ' + str(word1) + ' times;\n' \
        '\t' + str(self.searched_words[1]) + ' ' + str(word2) + ' times;\n' \
        '\t' + str(self.searched_words[2]) + ' ' + str(word3) + ' times;\n')


    def __str__(self):
        return '\nSite - %s' % (self.website)


if __name__ == '__main__':
    pass
