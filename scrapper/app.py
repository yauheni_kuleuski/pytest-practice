from .scrapper import Parser


def main():
    '''
    Some 20 vacancies. For scrap all - delete slice [30:50]'''
    session = Parser()
    if not session.website.search_pattern:
        session.appropriate_search_pattern()
    session.find_vacancies()
    print('\n' + str(len(session.enqueuer_urls)) + ' vacancies found.')
    for url in session.enqueuer_urls[30:50]:
        result = session.find_count_words(url)
        print(result)
        session.result_data_list.append(result)
    session.average_word_occurrence()

if __name__ == '__main__':
    main()