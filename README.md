Task1-jobs.tut.by-Parser
========================

Using requests lib for Pythons parse jobs.tut.by. Using of OOP approach is mandatory.

- Search job vacancies by a keyword (e.g python)
- Check all pages
- Count words "python", "linux", "flask" mentioned on each vacancy page
- Calculate average number of occurrence each word found

*Launch the application:*
```python
pip install -r requirements.txt
python scrapper/app.py
```
