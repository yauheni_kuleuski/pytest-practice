import pytest
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(scope="function")
def get_driver(request):
    driver: WebDriver = webdriver.Chrome(
        executable_path=ChromeDriverManager().install())
    request.cls.driver = driver
    yield
    driver.close()
